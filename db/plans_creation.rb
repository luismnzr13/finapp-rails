module PlansCreation
  class << self
    def perform
      Plan.create price: 750, name: "Monthly", period: 1
      Plan.create price: 687, name: "Yearly", period: 12
    end
  end
end
