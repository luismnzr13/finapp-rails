# frozen_string_literal: true

class CreatePayments < ActiveRecord::Migration[5.2]
  def change
    create_table :payments do |t|
      t.references :user, foreign_key: true
      t.decimal :amount, precision: 15, scale: 10
      t.string :payer, limit: 25
      t.string :currency_code
      t.datetime :payment_date
      t.string :subscr_id
      t.string :business
      t.string :item_name
      t.string :ipn_track_id
      t.timestamps
    end
  end
end
