class CreateEmbassadors < ActiveRecord::Migration[5.2]
  def change
    create_table :embassadors do |t|
      t.string :name
      t.string :email
      t.boolean :active, defaul: true

      t.timestamps
    end
  end
end
