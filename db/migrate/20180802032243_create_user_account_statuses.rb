# frozen_string_literal: true

class CreateUserAccountStatuses < ActiveRecord::Migration[5.2]
  def change
    create_table :user_account_statuses do |t|
      t.references :user, foreign_key: true
      t.string :state, null: false, default: 'normal'
      t.string :last_state
      t.datetime :last_payment_date
      t.datetime :expiration_date
      t.boolean :has_subscription, null: false, default: false
      t.string :subscription_type
      t.datetime :expiration_date_grace
      t.string :took_free_days, null: false, default: false
      t.timestamps
    end
  end
end
