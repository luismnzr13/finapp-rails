# frozen_string_literal: true

class CreatePaymentLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :payment_logs do |t|
      t.string :transaction_type
      t.references :user, foreign_key: true
      t.jsonb :payload,
              null: true,
              default: {},
              comment: 'All params of ipn'
      t.timestamps
    end
  end
end
