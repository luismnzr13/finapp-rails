class CreatePaymentSources < ActiveRecord::Migration[5.2]
  def change
    create_table :payment_sources do |t|
      t.string :provider
      t.string :token
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
