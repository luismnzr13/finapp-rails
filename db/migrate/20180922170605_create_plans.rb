class CreatePlans < ActiveRecord::Migration[5.2]
  def change
    create_table :plans do |t|
      t.integer :price
      t.integer :period
      t.string :name

      t.timestamps
    end
  end
end
