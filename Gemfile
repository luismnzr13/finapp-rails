# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.4.2'

gem 'activeadmin'
gem 'airrecord'
gem 'bcrypt', '~> 3.1.7'
gem 'bootsnap', '>= 1.1.0', require: false
gem 'coffee-rails', '~> 4.2'
gem 'conekta'
gem 'devise', '~> 4.2'
gem 'devise_campaignable'
gem 'gibbon'
gem 'high_voltage', '~> 3.1'
gem 'jbuilder', '~> 2.5'
gem 'jquery-rails'
gem 'omniauth-facebook'
gem 'omniauth-twitter'
gem 'paypal-sdk-rest'
gem 'pdfjs_viewer-rails'
gem 'pg'
gem 'puma', '~> 3.11'
gem 'rails', '~> 5.2.0'
gem 'rails_12factor', group: :production
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'

group :development, :test do
  gem 'brakeman', require: false
  gem 'bundler-audit', require: false
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'erb_lint'
  gem 'factory_bot_rails', '~> 4.8', '>= 4.8.2'
  gem 'ffaker'
  gem 'pry'
  gem 'pry-byebug'
  gem 'reek'
  gem 'rspec-rails', '~> 3.7'
  gem 'rubocop'
end

group :development do
  gem 'bullet'
  gem 'derailed_benchmarks'
  gem 'flamegraph'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'memory_profiler'
  gem 'meta_request'
  gem 'rack-mini-profiler'
  gem 'rails-erd'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'stackprof'
  gem 'traceroute'
  gem 'web-console', '>= 3.3.0'
end

group :test do
  gem 'capybara', '>= 2.15', '< 4.0'
  gem 'chromedriver-helper'
  gem 'database_cleaner'
  gem 'rails-controller-testing'
  gem 'rspec-collection_matchers'
  gem 'rubocop-rspec'
  gem 'selenium-webdriver'
  gem 'shoulda-matchers', '~> 3.1'
  gem 'simplecov', require: false
  gem 'simplecov-console', '~> 0.4.2', require: false
  gem 'simplecov-json', require: false
  gem 'simplecov-reporter', require: false
  gem 'warden-rspec-rails'
end

gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
