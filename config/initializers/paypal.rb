# frozen_string_literal: true

# config/initializers/paypal.rb

Rails.application.configure do
  config.x.paypal.host = ENV.fetch('PAYPAL_HOST', 'https://www.paypal.com')
  config.x.paypal.business = ENV.fetch('PAYPAL_BUSINESS', 'bernardo@vanfintec.com')
  config.x.paypal.hosted_button_id = ENV.fetch('PAYPAL_BUTTON_ID', 'N5RSN8MPJRWGL')
  config.x.paypal.merchant_account_id = ENV.fetch('PAYPAL_MERCHANT_ACCOUNT_ID', 'R622XGZBP99QJ')
end
