# frozen_string_literal: true

HighVoltage.configure do |config|
  config.routes = false
  # config.content_path = 'edu/'
  config.layout = 'application'
end
