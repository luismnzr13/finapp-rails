# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }

  get '/pages/*id' => 'pages#show', as: :page, format: false

  root 'main#index'

  mount PdfjsViewer::Rails::Engine => '/pdfjs', as: 'pdfjs'

  get 'main/senales', :as => 'senales_main'
  get 'main/educacion', :as => 'educacion_main'
  get 'main/plans', :as => 'plans_main'
  get 'main/premium', :as => 'premium_main'
  get 'main/welcome', :as => 'welcome_main'
  get 'main/ayuda', :as => 'ayuda_main'

  get 'dashboard/index', :as => 'dashboard_index'
  get 'dashboard/educacion', :as => 'dashboard_educacion'
  get 'dashboard/senales', :as => 'dashboard_senales'
  get 'dashboard/info', :as => 'dashboard_info'

  #Articles
  get 'academy/articulo', :as => 'academy_articulo'
  get 'academy/analisisTecnico', :as => 'academy_analisisTecnico'
  get 'academy/analisisTecnicoL1', :as => 'academy_analisisTecnicoL1'
  get 'academy/analisisTecnicoL2', :as => 'academy_analisisTecnicoL2'
  get 'academy/analisisTecnicoL3', :as => 'academy_analisisTecnicoL3'
  get 'academy/analisisTecnicoL4', :as => 'academy_analisisTecnicoL4'
  get 'academy/analisisTecnicoL5', :as => 'academy_analisisTecnicoL5'
  get 'academy/analisisTecnicoL6', :as => 'academy_analisisTecnicoL6'
  get 'academy/bancosCentrales', :as => 'academy_bancosCentrales'
  get 'academy/acciones', :as => 'academy_acciones'
  get 'academy/bitcoin', :as => 'academy_bitcoin'
  get 'academy/materias', :as => 'academy_materias'
  get 'academy/correlacion', :as => 'academy_correlacion'
  get 'academy/sentimiento', :as => 'academy_sentimiento'

  #Archive
  get 'archive/amxl', :as => 'archive_amxl'
  get 'archive/femsaubd', :as => 'archive_femsaubd'
  get 'archive/gfnorteo', :as => 'archive_gfnorteo'
  get 'archive/walmex', :as => 'archive_walmex'
  get 'archive/gmexicob', :as => 'archive_gmexicob'
  get 'archive/cemexcpo', :as => 'archive_cemexcpo'
  get 'archive/tlevisacpo', :as => 'archive_tlevisacpo'
  get 'archive/alfaa', :as => 'archive_alfaa'
  get 'archive/gapb', :as => 'archive_gapb'
  get 'archive/asurb', :as => 'archive_asurb'
  get 'archive/kofl', :as => 'archive_kofl'
  get 'archive/mexchem', :as => 'archive_mexchem'
  get 'archive/ac', :as => 'archive_ac'
  get 'archive/kimbera', :as => 'archive_kimbera'
  get 'archive/gfinburo', :as => 'archive_gfinburo'
  get 'archive/bimboa', :as => 'archive_bimboa'
  get 'archive/ienova', :as => 'archive_ienova'
  get 'archive/grumab', :as => 'archive_grumab'
  get 'archive/bsmxb', :as => 'archive_bsmxb'
  get 'archive/pinfra', :as => 'archive_pinfra'
  get 'archive/penoles', :as => 'archive_penoles'
  get 'archive/elektra', :as => 'archive_elektra'
  get 'archive/omab', :as => 'archive_omab'
  get 'archive/alsea', :as => 'archive_alsea'
  get 'archive/megacpo', :as => 'archive_megacpo'
  get 'archive/liverpolc1', :as => 'archive_liverpolc1'
  get 'archive/gcarsoa1', :as => 'archive_gcarsoa1'
  get 'archive/cuervo', :as => 'archive_cuervo'
  get 'archive/gmxt', :as => 'archive_gmxt'
  get 'archive/lalab', :as => 'archive_lalab'
  get 'archive/gentera', :as => 'archive_gentera'
  get 'archive/volara', :as => 'archive_volara'
  get 'archive/ra', :as => 'archive_ra'
  get 'archive/alpeka', :as => 'archive_alpeka'
  get 'archive/nemaka', :as => 'archive_nemaka'
  get 'archive/bbajioo', :as => 'archive_bbajioo'
  get 'archive/gcc', :as => 'archive_gcc'

  get 'podcast/index', :as => 'podcast_index'

  resources :posts

  match 'payments/cancel/view' => 'payments#cancel_view', via: :all
  match 'payments/success/view' => 'payments#success_view', via: :all
  match 'payments/webhook' => 'payments#webhook', via: :all
  get 'payment/:payment', to: "payments#show", as: :payment

  post 'payments', to: "payments#create"
  post 'conekta', to: "payments#conekta"

  get 'profile' => 'users#profile', as: :profile
end
