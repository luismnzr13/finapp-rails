# FinApp

## Table of contents

* [Run With Docker](#run-with-docker)

## Run With Docker
If you will run this app for first time, you have to run db migrations and update gems  
```
docker-compose run web bundle install
docker-compose run web rails db:create db:migrate

```
Run app
```
docker-compose up -d web

```
