class SubscriptionsMailerPreview < ActionMailer::Preview
  def notify
    user = PaymentSource.last.user
    data_information = OpenStruct.new card_holder_name: "Fake User Name",
                                      card_last_digits: "1234",
                                      status: "paid",
                                      user_email: user.email,
                                      user_name: user.name

    SubscriptionsMailer.notify(data_information)
  end
end
