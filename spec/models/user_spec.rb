# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  it { is_expected.to have_many :payment_sources }
  it { is_expected.to have_one :account }
  it { is_expected.to respond_to :account? }
  it { is_expected.to respond_to :password_required? }
  it { is_expected.to delegate_method(:state).to(:account) }
  it { expect(described_class).to respond_to :new_with_session }
  it { expect(described_class).to respond_to :from_omniauth }
end
