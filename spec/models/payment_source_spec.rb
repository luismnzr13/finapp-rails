require 'rails_helper'

RSpec.describe PaymentSource, type: :model do
  it { is_expected.to respond_to :provider }
  it { is_expected.to respond_to :token}

  it { is_expected.to belong_to :user }
end
