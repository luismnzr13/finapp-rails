require 'rails_helper'

RSpec.describe Plan, type: :model do
  it { is_expected.to respond_to :price }
  it { is_expected.to respond_to :period }
  it { is_expected.to respond_to :name }
  it { is_expected.to respond_to :decimal_price }
end
