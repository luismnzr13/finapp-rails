# frozen_string_literal: true

require 'simplecov'
require 'simplecov-console'
require 'simplecov-reporter'
require 'simplecov-json'

SimpleCov.formatter = SimpleCov::Formatter::MultiFormatter.new [
  SimpleCov::Formatter::HTMLFormatter
]

SimpleCov.start 'rails' do
  add_filter [/spec/, /config/, /rescue_error/, /jsonb_type/, /_helper/, /exceptions/, /api_constraints/, /concerns/, /custom_token/]
end
