FactoryBot.define do
  factory :coupon do
    name { FFaker::Product.brand }
    code { SecureRandom.hex(16) }
    embassador
  end
end
