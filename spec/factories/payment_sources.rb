FactoryBot.define do
  factory :payment_source do
    token { SecureRandom.hex(16) }

    trait :with_paypay do
      provider { "paypay" }
    end

    trait :with_conekta do
      provider { "conekta" }
    end

    factory :paypal_payment_source, traits: [:with_paypay]
    factory :conekta_payment_source, traits: [:with_conekta]
  end
end
