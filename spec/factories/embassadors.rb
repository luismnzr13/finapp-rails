FactoryBot.define do
  factory :embassador do
    name { FFaker::Name.name }
    email { FFaker::Internet.email }

    trait :as_active do
      active { true }
    end

    trait :as_deactive do
      active { false }
    end

    factory :active_embassador, traits: [:as_active]
    factory :deactive_embassador, traits: [:as_deactive]

  end
end
