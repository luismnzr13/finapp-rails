# frozen_string_literal: true

FactoryBot.define do
  factory :user_account_status do
    association :user, factory: :user
  end
end
