# frozen_string_literal: true

require 'rails_helper'

# Specs in this file have access to a helper object that includes
# the PaymentsHelper. For example:
#
# describe PaymentsHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe PaymentsHelper, type: :helper do
  let!(:user) do
    allow_any_instance_of(Devise::Models::Campaignable::Adapters::Mailchimp).to receive(:subscribe).and_return(true)
    create :user
  end
  let!(:account) { create :user_account_status, user: user }

  let!(:paid_and_subs_user) do
    user.account.update state: 'premium',
                        has_subscription: true,
                        subscription_type: 'M',
                        last_payment_date: Date.today,
                        expiration_date: Date.today + 1.month
  end

  let!(:subs_cancel_not_expired) do
    user.account.update state: 'normal',
                        last_state: 'premium',
                        has_subscription: false,
                        expiration_date: Date.today + 1.day,
                        last_payment_date: Date.today - 1.month + 1.day
  end

  let!(:subs_not_paid_and_free_days) do
    user.account.update state: 'normal',
                        last_state: 'premium',
                        has_subscription: false,
                        expiration_date: Date.today - 1.day,
                        last_payment_date: Date.today - 1.month - 1.day,
                        took_free_days: true,
                        expiration_date_grace: Date.today + 14.days
  end

  let!(:subs_not_paid_and_not_free_days) do
    user.account.update state: 'normal',
                        last_state: 'premium',
                        has_subscription: false,
                        expiration_date: Date.today - 2.months,
                        last_payment_date: Date.today - 2.months,
                        took_free_days: true,
                        expiration_date_grace: Date.today - 2.months + 14.days
  end

  describe 'User can pay' do
    it 'as normal user' do
      sign_in user
      expect(helper.user_can_pay?).to be true
    end
  end
end
