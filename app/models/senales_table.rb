# frozen_string_literal: true

Airrecord.api_key = 'keydqCTarrKLQaUog'

class Tea < Airrecord::Table
  self.base_key = 'app7PUTJl2mcKxQ3m'
  self.table_name = 'Senales'

  def location
    [self[:village], self[:country], self[:region]].compact.join(', ')
  end
end
