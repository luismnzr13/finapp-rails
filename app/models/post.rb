# frozen_string_literal: true

class Post < ApplicationRecord
  belongs_to :category
  validates :title, :content, :category_id, presence: true
  default_scope { order(created_at: :desc) }
end
