# frozen_string_literal: true

class UserAccountStatus < ApplicationRecord
  ACCOUNT_STATES = %w[premium pro].freeze

  belongs_to :user, inverse_of: :account

  def subscription?
    has_subscription && !expired?
  end

  def subscription_grace?
    took_free_days && !expired_date_grace? && (ACCOUNT_STATES.include? last_state)
  end

  def any_subscription?
    subscription? || subscription_grace?
  end

  def premium?
    state? 'premium'
  end

  def pro?
    state? 'pro'
  end

  def state?(state_type)
    state == state_type || (last_state == state_type && !account_expired?)
  end

  def account_expired?
    expired? || expired_date_grace?
  end

  def expired?
    Time.zone.today > expiration_date
  end

  def expired_date_grace?
    return true if expiration_date_grace.nil?

    Time.zone.today > expiration_date_grace
  end
end
