class Coupon < ApplicationRecord
  has_secure_token :code
  belongs_to :embassador

  accepts_nested_attributes_for :embassador
end
