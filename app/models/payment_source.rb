# frozen_string_literal: true

class PaymentSource < ApplicationRecord
  belongs_to :user
end
