# frozen_string_literal: true

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :campaignable,
         :omniauthable, omniauth_providers: %i[facebook twitter]

  has_one :account, class_name: 'UserAccountStatus', dependent: :destroy
  has_many :payment_sources, dependent: :destroy

  delegate :state, to: :account

  class << self
    def new_with_session(params, session)
      super.tap do |user|
        data = session['devise.facebook_data']
        if data.present? && session['devise.facebook_data']['extra']['raw_info']
          user.email = data['email'] if user.email.blank?
        end
      end
    end

    def from_omniauth(auth)
      where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
        user.email = auth.info.email
        user.password = Devise.friendly_token[0, 20]
        user.name = auth.info.name # assuming the user model has a name
      end
    end
  end

  def account?
    account.present?
  end

  def password_required?
    super && provider.blank?
  end
end
