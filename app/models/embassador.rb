class Embassador < ApplicationRecord
  has_one :coupon
  accepts_nested_attributes_for :coupon
end
