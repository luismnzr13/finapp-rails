# frozen_string_literal: true

class Plan < ApplicationRecord
  def decimal_price
    price / 100.00
  end
end
