const AIRTABLE_ID5 = "appF7SgELGraYBQ99";
const BASE_URL5 =
  "https://api.airtable.com/v0/" +
  AIRTABLE_ID5 +
  "/Ratios?view=Grid%20view";

class RatiosApi {
  static fetchRatios(filterByFormula) {
    return fetch(BASE_URL5 + filterByFormula, {
      headers: {
        Authorization: `Bearer ${AIRTABLE_API_KEY}`
      }
    })
      .then(this.json)
      .then(data => {
        return this.convertDataToRatios(data);
        console.log("Request succeeded with JSON response", data);
      })
      .catch(function(error) {
        console.log("Request failed", error);
      });
  }

  static convertDataToRatios(data) {
    return data.records.map(record => {
      return this.convertRecordToRatio(record);
    });
  }

  static convertRecordToRatio(record) {
    const fields = record.fields;
    return {
      company: fields["Emisora"],
      particip: fields["Participacion"],
      pu: fields["P/U"],
      pvl: fields["P/VL"],
      evebitda: fields["EV/EBITDA"],
      upa: fields["UPA"],
      dnpn: fields["DN/PN"],
      roic: fields["ROIC"],
      beta: fields["BETA"]
    };
  }

  static json(response) {
    return response.json();
  }
}

const getRatioMarkup = ratio => {
  return `<tr>
            <th>${ratio.company}</th>
            <td>${ratio.particip}</td>
            <td>${ratio.pu}</td>
            <td>${ratio.pvl}</td>
            <td>${ratio.evebitda}</td>
            <td>${ratio.upa}</td>
            <td>${ratio.dnpn}</td>
            <td>${ratio.roic}</td>
            <td>${ratio.beta}</td>
          </tr>`;
};
// END Ratio markup

const loadRatios = (filterByFormula = "") => {
  RatiosApi.fetchRatios(filterByFormula).then(ratios => {
    $("tbody.profile__intro-tableRatios").html("");
    ratios.forEach(ratio => {
      $("tbody.profile__intro-tableRatios").append(getRatioMarkup(ratio));
    });
  });
};

loadRatios();
