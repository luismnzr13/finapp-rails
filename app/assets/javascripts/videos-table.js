const AIRTABLE_ID2 = "appGkVB8m0bacHx7a";
const BASE_URL2 =
  "https://api.airtable.com/v0/" +
  AIRTABLE_ID2 +
  "/Videos-info?sort%5B0%5D%5Bfield%5D=Fecha&sort%5B0%5D%5Bdirection%5D=desc";

class VideosApi {
  static fetchVideos(filterByFormula) {
    return fetch(BASE_URL2 + filterByFormula, {
      headers: {
        Authorization: `Bearer ${AIRTABLE_API_KEY}`
      }
    })
      .then(this.json)
      .then(data => {
        return this.convertDataToVideos(data);
        console.log("Request succeeded with JSON response", data);
      })
      .catch(function(error) {
        console.log("Request failed", error);
      });
  }

  static convertDataToVideos(data) {
    return data.records.map(record => {
      return this.convertRecordToVideo(record);
    });
  }

  static convertRecordToVideo(record) {
    const fields = record.fields;
    return {
      title: fields["Titulo"],
      description: fields["Descripcion"],
      date: fields["Fecha"],
      duration: fields["Duracion"],
      videoId: fields["Video-id"]
    };
  }

  static json(response) {
    return response.json();
  }
}

const getVideoMarkup = video => {
  return `<div class="profile__intro-videos-row">
            <div class="row profile__education">
              <div class="six columns">
                <div class="plyr__video-embed" id="player">
                ${video.videoId}
                </div>
              </div>
              <div class="six columns ">
                <p class="profile__intro-videoTitle">${video.title}</p>
                <h5 class="profile__intro-subtitle">${video.date}</h5>
                <p class="profile__intro-text">${video.description}</p>
                <p class="profile__intro-date">${video.duration}</p>
              </div>
            </div>
          </div>`;
};
// END Video markup

const loadVideos = (filterByFormula = "") => {
  VideosApi.fetchVideos(filterByFormula).then(videos => {
    $("div.dashboard__videos").html("");
    videos.forEach(video => {
      $("div.dashboard__videos").append(getVideoMarkup(video));
    });
  });
};

loadVideos();
