const AIRTABLE_ID3 = "appoUOrEoX7yBpuKd";
const BASE_URL3 =
  "https://api.airtable.com/v0/" +
  AIRTABLE_ID3 +
  "/Comments?sort%5B0%5D%5Bfield%5D=Fecha&sort%5B0%5D%5Bdirection%5D=desc";

class CommentsApi {
  static fetchComments(filterByFormula) {
    return fetch(BASE_URL3 + filterByFormula, {
      headers: {
        Authorization: `Bearer ${AIRTABLE_API_KEY}`
      }
    })
      .then(this.json)
      .then(data => {
        return this.convertDataToComments(data);
        console.log("Request succeeded with JSON response", data);
      })
      .catch(function(error) {
        console.log("Request failed", error);
      });
  }

  static convertDataToComments(data) {
    return data.records.map(record => {
      return this.convertRecordToComment(record);
    });
  }

  static convertRecordToComment(record) {
    const fields = record.fields;
    return {
      title: fields["Tema"],
      update: fields["Comentario"],
      date: fields["Fecha"],
      id: fields["ID"]
    };
  }

  static json(response) {
    return response.json();
  }
}

const getCommentMarkup = comment => {
  return `<div class="profile__intro-videos-row">
            <div class="row profile__education">
              <div class="twelve columns">
                <h5 class="profile__intro-subtitle">${comment.date}</h5>
                <h4 class="profile__intro-videoTitle">${comment.title}</h4>
                <span class="clickable">Leer <i class="fas fa-chevron-down"></i></span>
                <p class="dashboard__updates-comment" style="display:none" id="${comment.id}">${comment.update}</p>
                <hr>
              </div>
            </div>
          </div>`;
};
// END Comment markup

const loadComments = (filterByFormula = "") => {
  CommentsApi.fetchComments(filterByFormula).then(comments => {
    $("div.dashboard__updates").html("");
    comments.forEach(comment => {
      $("div.dashboard__updates").append(getCommentMarkup(comment));
    });
  });
};

loadComments();

function toggleDocs(event) {

    if (event.target && event.target.className == 'clickable') {

        var next = event.target.nextElementSibling;


        if (next.style.display == "none") {
            next.style.display = "block";

        } else {
            next.style.display = "none";
        }
    }
}

document.addEventListener('click', toggleDocs, true);
