const AIRTABLE_ID = "app7PUTJl2mcKxQ3m";
const AIRTABLE_API_KEY = "keydqCTarrKLQaUog";
const BASE_URL =
  "https://api.airtable.com/v0/" +
  AIRTABLE_ID +
  "/senales_finapp?sort%5B0%5D%5Bfield%5D=Fecha&sort%5B0%5D%5Bdirection%5D=desc";

class SignalsApi {
  static fetchSignals(filterByFormula) {
    return fetch(BASE_URL + filterByFormula, {
      headers: {
        Authorization: `Bearer ${AIRTABLE_API_KEY}`
      }
    })
      .then(this.json)
      .then(data => {
        return this.convertDataToSignals(data);
        console.log("Request succeeded with JSON response", data);
      })
      .catch(function(error) {
        console.log("Request failed", error);
      });
  }

  static convertDataToSignals(data) {
    return data.records.map(record => {
      return this.convertRecordToSignal(record);
    });
  }

  static convertRecordToSignal(record) {
    const fields = record.fields;
    return {
      company: fields["Emisora"],
      entryPrice: fields["Entrada"],
      stopLoss: fields["Stop Loss"],
      objectiveOne: fields["Objetivo 1"],
      objectiveTwo: fields["Objetivo 2"],
      objectiveThree: fields["Objetivo 3"],
      date: fields["Fecha"],
      time: fields["Hora"],
      imageUrl: this.getGraphImage("Grafico", fields)
    };
  }

  static json(response) {
    return response.json();
  }

  static getGraphImage(graphName, fields) {
    if (fields[`${graphName} Image`] && fields[`${graphName} Image`][0]) {
      return fields[`${graphName} Image`][0].url;
    }
  }
}

const getSignalMarkup = signal => {
  return `<tr>
            <td>
              <p class="signal-company">${signal.company}</p>
              </td>
              <td>
              <p>${signal.date}</p>
              <p class="signal-date">${signal.time}</p>
              </td>
              <td>${signal.entryPrice}</td>
              <td>
              <p class="signal-loss">${signal.stopLoss}</p>
              </td>
              <td>
              <p>${signal.objectiveOne}</p>
              </td>
              <td>
              <p>${signal.objectiveTwo}</p>
              </td>
              <td>
              <p>${signal.objectiveThree}</p>
            </td>
            <td>
            <a data-lightbox="signalGraph" href="${signal.imageUrl}">
            <img id="myImg" class="signal-graph" src="${
              signal.imageUrl
            }" alt="">
            </td>
            </a>
          </tr>`;
};
// END Signal markup

const loadSignals = (filterByFormula = "") => {
  SignalsApi.fetchSignals(filterByFormula).then(signals => {
    $("tbody.profile__intro-tableBody").html("");
    signals.forEach(signal => {
      $("tbody.profile__intro-tableBody").append(getSignalMarkup(signal));
    });
  });
};

loadSignals();
