var parent = document.querySelector('meta[name="pdfjs_viewer_verbosity"]');
if (parent != null) {
  var verbosity = parent.content;
  PDFJS.verbosity = PDFJS.VERBOSITY_LEVELS[verbosity];
  PDFJS.externalLinkTarget = PDFJS.LinkTarget.BLANK
}
