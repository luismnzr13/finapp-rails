# frozen_string_literal: true

ActiveAdmin.register Plan do
  permit_params :price, :period, :name

  index do
    selectable_column
    id_column
    column :name
    column :price
    column :period
    column :created_at
    actions
  end

  filter :name
  filter :price
  filter :period
  filter :created_at

  form do |f|
    f.inputs do
      f.input :name
      f.input :price
      f.input :period
    end
    f.actions
  end
end
