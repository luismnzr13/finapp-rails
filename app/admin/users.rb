# frozen_string_literal: true

ActiveAdmin.register User do
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  permit_params :name, :email, :password, :password_confirmation

  form title: 'New user' do |f|
    inputs 'Details' do
      input :name
      input :email
      input :password, label: 'Password'
      input :password_confirmation, label: 'Confirm Password'
      li "Created at #{f.object.created_at}" unless f.object.new_record?
    end
    panel 'Markup' do
      'The following can be used in the content below...'
    end
    para 'Press cancel to return to the list without saving.'
    actions
  end

  index do
    id_column
    column :name
    column :email
    actions
  end

  show do
    h3 user.name
    h3 user.email
  end
end
