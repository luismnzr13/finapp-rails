# coding: utf-8
# frozen_string_literal: true

ActiveAdmin.register Embassador do
  permit_params :name, :email, :active, coupon_attributes: [:name]

  index do
    selectable_column
    id_column
    column :name
    column :email
    column :active
    column :coupon do |e|
      e.coupon&.code
    end
    column :created_at
    actions
  end

  filter :name
  filter :email
  filter :active
  filter :created_at

  form do |f|
    f.inputs do
      f.input :name
      f.input :email
      f.input :active

      f.has_many :coupon, heading: 'Coupon',
                 new_record: true do |c|
        c.input :name
      end
    end
    f.actions
  end
end
