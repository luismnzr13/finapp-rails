# frozen_string_literal: true

ActiveAdmin.register Coupon do
  permit_params :name, :embassador_id, embassador_attributes: [:id, :name]

  index do
    selectable_column
    id_column
    column :name
    column :code
    column :created_at
    actions
  end

  filter :name
  filter :code
  filter :created_at

  form do |f|
    f.inputs do
      f.input :name
      f.has_many :embassador, heading: 'Embassador',
                 new_record: false do |e|
        e.input :id
      end
    end
    f.actions
  end
end
