# frozen_string_literal: true

ActiveAdmin.register UserAccountStatus do
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  permit_params :user_id, :state, :last_payment_date, :expiration_date, :has_subscription

  form title: 'New user status' do |f|
    inputs 'Details' do
      input :user_id
      input :state
      input :last_payment_date
      input :expiration_date
      input :has_subscription
      li "Created at #{f.object.created_at}" unless f.object.new_record?
    end
    panel 'Markup' do
      'The following can be used in the content below...'
    end
    para 'Press cancel to return to the list without saving.'
    actions
  end
end
