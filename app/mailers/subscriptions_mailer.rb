# coding: utf-8
class SubscriptionsMailer < ApplicationMailer
  def notify(data)
    @data_information = data
    mail(to: @data_information.user_email, subject: 'Cargo de Subscripción')
  end
end
