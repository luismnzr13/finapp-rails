# frozen_string_literal: true

class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
    update_attrs = [:name, :password, :password_confirmation, :current_password]
    devise_parameter_sanitizer.permit :account_update, keys: update_attrs
  end

  def user_has_any_subscription!
    unless any_subscription?
      redirect_to root_path
      return false
    end
    true
  end

  private

  def any_subscription?
    current_user.account? && current_user.account.any_subscription?
  end
end
