# frozen_string_literal: true

class DashboardController < ApplicationController
  before_action :authenticate_user!, :user_has_any_subscription!

  def index; end

  private
end
