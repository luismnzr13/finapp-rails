# frozen_string_literal: true

class PodcastController < ApplicationController
  before_action :authenticate_user!

  def index; end
end
