# frozen_string_literal: true

class AcademyController < ApplicationController
  before_action :authenticate_user!, :user_has_any_subscription!

  def index; end
end
