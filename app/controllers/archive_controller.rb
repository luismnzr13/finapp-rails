# frozen_string_literal: true

class ArchiveController < ApplicationController
  before_action :authenticate_user!, :user_has_any_subscription!

  def index; end
end
