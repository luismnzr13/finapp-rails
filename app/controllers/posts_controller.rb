# frozen_string_literal: true

class PostsController < ApplicationController
  before_action :set_post, only: %i[show edit update destroy]

  def index
    @posts = post_scope
  end

  def show; end

  def new
    @post = Post.new
  end

  def create
    @post = Post.new(post_params)
    if @post.save
      redirect_to @post, notice: 'The post was created!'
    else
      render 'new'
    end
  end

  def edit; end

  def update
    if @post.update(post_params)
      redirect_to @post, notice: 'Update successful'
    else
      render 'edit'
    end
  end

  def destroy
    @post.destroy
    redirect_to root_path, notice: 'Post destroyed'
  end

  private

  def post_params
    params.require(:post).permit(:title, :content, :category_id)
  end

  def set_post
    @post = Post.find(params[:id])
  end

  def params_category_conditions
    params.slice(:category_name)
          .transform_keys { |key| key.gsub 'category_', '' }
          .permit.to_h
  end

  def post_scope
    %i[by_category].reduce(Post.all) do |scope, reducer|
      send "scope_#{reducer}".to_sym, scope
    end
  end

  def scope_by_category(scope)
    return scope unless params_category_conditions.any?

    scope.joins(:category).merge Category.where params_category_conditions
  end
end
