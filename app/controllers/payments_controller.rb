# coding: utf-8
# frozen_string_literal: true

class PaymentsController < ApplicationController
  before_action :authenticate_user!, only: %i[success_view create]
  skip_before_action :verify_authenticity_token, only: %i[webhook cancel_view success_view conekta]

  def show
    @payment = params[:payment]
  end

  def webhook
    Rails.logger.info "\n\n\n\n#{params}\n\n\n\n"
    payment_service = PaypalServiceBase.service(payment_params)
    payment_service.exec
    head :no_content
  end

  def conekta
    data = JSON.parse(request.body.read)
    case data['type']
    when 'subscription.paid'
      user = PaymentSource.find_by(token: data['data']['object']['customer_id']).user
      data_information = OpenStruct.new card_holder_name: data['data']['object']['payment_method']['name'],
                                        card_last_digits: data['data']['object']['payment_method']['last4'],
                                        status: data['data']['object']['status'],
                                        user_email: user.email,
                                        user_name: user.name
      SubscriptionsMailer.notify data_information
    end

    render status: 200
  rescue
    render status: 200
  end

  # TODO: create view
  def cancel_view
    redirect_to educacion_main_path
  end

  def success_view
    redirect_to welcome_main_path
  end

  def create
    case params[:source]
    when 'conekta'
      ConektaService
        .subscription(conekta_params, current_user)
    end

    redirect_to root_path, notice: "Tu subscripción se ha creado con éxito!"
  end

  private

  # Now all params are accepted, but this method could be change
  # if we want filter some param
  def payment_params
    params.except(:action, :controller)
  end

  def conekta_params
    params.permit!
  end
end
