# frozen_string_literal: true

module ApplicationHelper
  def monthly_plan
    plan = Plan.find_by(name: "Monthly")
    if plan
      "$#{plan.decimal_price}mxn/Mes"
    else
      "$750mxn/Mes"
    end
  end

  def yearly_plan
    plan = Plan.find_by(name: "Yearly")
    if plan
      "$#{plan.decimal_price}mxn/Mes"
    else
      "$687mxn/Mes"
    end
  end
end
