# frozen_string_literal: true

module PaymentsHelper
  def user_can_pay?
    !current_user.nil? && (!current_user.account? || !current_user.account.any_subscription?)
  end
end
