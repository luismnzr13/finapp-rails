# frozen_string_literal: true

module Errors
  module RescueError
    def self.included(base)
      base.rescue_from Exceptions::PaymentError do |e|
        if request.present?
          case request.format
          when :json then render json: { message: "Payment failed: #{e}" }, status: 489
          end
        end
      end

      base.rescue_from Exceptions::CardFailedError do |e|
        if request.present?
          case request.format
          when :json then render json: { message: "Card failed: #{e}" }, status: 489
          end
        end
      end
    end
  end
end
