# frozen_string_literal: true

module Plans
  class << self
    def price(plan_name)
      plan = Plan.find_by(name: plan_name)
      raise "No plan found." unless plan
      plan.decimal_price
    end

    def period(plan_name)
      plan = Plan.find_by(name: plan_name)
      raise "No plan found." unless plan
      plan.period
    end
  end
end
