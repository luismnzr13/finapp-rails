# frozen_string_literal: true

module Exceptions
  class FinappError < StandardError; end
  class CardFailedError < FinappError
    def initialize(msg)
      super(msg)
    end
  end
  class PaymentError < FinappError
    def initialize(msg)
      super(msg)
    end
  end
end
