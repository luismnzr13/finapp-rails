# frozen_string_literal: true

module Coupons
  class << self
    def discount(coupon_code, plan_name)
      plan = Plan.find_by(name: plan_name)
      raise "No plan found." unless plan
      coupon = Coupon.find_by(code: coupon_code)
      if coupon && coupon.embassador&.active
        coupon.discount * plan.price
      else
        0
      end
    end
  end
end
