# frozen_string_literal: true

#= PaypalDefaultService
class PaypalDefaultService < PaypalServiceBase
  def exec
    transaction do
      super
    end
  end
end
