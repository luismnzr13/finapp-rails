# frozen_string_literal: true

module ConektaService
  module Customer
    class << self
      def create(params, user)
        conekta_user = conekta_customer_id user
        if conekta_user.blank?
          customer = Conekta::Customer.create ConektaService::CustomerParams.reformat(params, user)
          customer
        else
          conekta_user
        end
      rescue Conekta::Error => exception
        ConektaService::Loggable.to_console exception
        raise StandardError, exception.details.map(&:message).join('.')
      end

      def conekta_customer_id(user)
        payment_sources = user.payment_sources.where provider: 'conekta'
        return nil if payment_sources.blank?

        payment_sources.each do |payment_source|
          begin
            customer = Conekta::Customer.find payment_source.token
          rescue Conekta::Error
            next
          end
          return customer if customer.present?
        end
      end
    end
  end
end
