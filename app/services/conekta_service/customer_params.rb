# frozen_string_literal: true

module ConektaService
  module CustomerParams
    class << self
      def reformat(params, user)
        {
          name: user.name,
          email: user.email,
          phone: params[:payment][:telephone],
          payment_sources: [{
                              token_id: params[:payment][:token],
                              type: "card"
                            }]
        }
      end
    end
  end
end
