# frozen_string_literal: true

module ConektaService
  module Plan
    class << self
      def create(params)
        Conekta::Plan.create(params)
      end
    end
  end
end
