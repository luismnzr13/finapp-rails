# frozen_string_literal: true

module ConektaService
  module Loggable
    class << self
      def to_console(exception)
        exception.details.each do |detail|
          Rails.logger.info detail.message
        end
      end
    end
  end
end
