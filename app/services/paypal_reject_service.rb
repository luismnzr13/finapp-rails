# frozen_string_literal: true

#= PaypalRejectService
class PaypalRejectService < PaypalServiceBase
  def exec
    transaction do
      super
      check_free_days
    end
  end

  def check_free_days
    a = user.account.took_free_days
    if a == 'f'
      user.account.assign_attributes user_account_attrs
      user.account.save
    end
  end

  def user_account_attrs
    {
      user_id: user.id,
      expiration_date_grace: expiration_date_grace,
      took_free_days: true
    }
  end

  def expiration_date_grace
    user.account.expiration_date + 15.days
  end
end
