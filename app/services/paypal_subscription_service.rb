# frozen_string_literal: true

#= PaypalSubscriptionService
class PaypalSubscriptionService < PaypalServiceBase
  def exec
    transaction do
      super
      save_user_account
    end
  end
end
