# frozen_string_literal: true

#= PaypalPaymentService
class PaypalPaymentService < PaypalServiceBase
  PAY_PAY_DATE_FORMAT = '%H:%M:%S %B %d, %Y %Z'

  def exec
    transaction do
      super
      save_payment
      save_user_account
    end
  end

  def user_account_attrs
    attrs = {
      user_id: user.id,
      state: 'premium',
      last_payment_date: payment_date,
      has_subscription: true
    }

    attrs[:expiration_date] = if subscription_monthly?
                                payment_date + 1.month
                              else
                                payment_date + 1.year
                              end
    attrs
  end

  def save_payment
    @payment = Payment.create(payment_attrs)
  end

  def payment_date
    @payment_date ||= Date.strptime(@params[:payment_date], PAY_PAY_DATE_FORMAT)
  end

  def payment_attrs
    {
      amount: @params[:mc_gross],
      payer: @params[:payer_id],
      currency_code: @params[:mc_currency],
      payment_date: payment_date,
      subscr_id: @params[:subscr_id],
      business: @params[:business],
      ipn_track_id: @params[:ipn_track_id],
      user_id: @params[:custom],
      item_name: @params[:item_name]
    }
  end
end
