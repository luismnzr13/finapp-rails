# frozen_string_literal: true

#= PaypalService
class PaypalServiceBase
  BUSINESS = Rails.configuration.x.paypal.business
  PAYPAL_HOST = Rails.configuration.x.paypal.host
  HOSTED_BUTTON_ID = Rails.configuration.x.paypal.hosted_button_id
  CURRENCY_CODE = 'MXN'
  MERCHANT_ACCOUNT_ID = Rails.configuration.x.paypal.merchant_account_id

  SUBSCRIPTION_TYPES = {
    "1 M": 'M',
    "1 Y": 'A'
  }.freeze

  PAY_PAY_DATE_FORMAT = '%H:%M:%S %B %d, %Y %Z'

  TNX_TYPES = {
    rejected: %w[failed rejected],
    cancel: %w[subscr_cancel cancel],
    payment: %w[subscr_payment payment failed],
    subscription: %w[subscr_signup subscription]
  }.freeze

  delegate :transaction, to: ActiveRecord::Base

  def self.form_url
    "#{PaypalServiceBase::PAYPAL_HOST}/cgi-bin/webscr"
  end

  def self.service(params)
    PaypalServiceBase.service_class(params[:txn_type], params[:payment_status]).new(params)
  end

  def self.service_class(txn_type, payment_status)
    type = PaypalServiceBase.transaction_type(txn_type, payment_status)
    "Paypal#{type.camelize}Service".constantize
  end

  def self.transaction_type(txn_type, payment_status = 'completed')
    txn_type = txn_type.downcase
    if TNX_TYPES[:rejected].include?(txn_type) then 'rejected'
    elsif TNX_TYPES[:cancel].include?(txn_type) then 'cancel'
    elsif TNX_TYPES[:subscription].include?(txn_type) then 'subscription'
    elsif TNX_TYPES[:payment].include?(txn_type)
      if payment_status.casecmp('completed').zero?
        'payment'
      else
        'reject'
      end
    else 'default'
    end
  end

  def initialize(params)
    @params = params
  end

  def exec
    save_payment_log
  end

  def payment_completed?
    payment_status == 'completed'
  end

  def payment_status
    @payment_status ||= @params[:payment_status].downcase
  end

  def save_user_account
    user.account = UserAccountStatus.new \
    if user.account.nil?
    user.account.assign_attributes user_account_attrs
    user.account.save
  end

  def user_account_attrs
    attrs = {
      user_id: user.id,
      state: subscription? ? 'premium' : 'normal',
      subscription_type: subscription_type
    }
    if subscription?
      attrs[:last_payment_date] = payment_date
      attrs[:expiration_date] = expiration_date
      attrs[:has_subscription] = true
    end
    attrs
  end

  def save_payment_log
    @payment_log = PaymentLog.create(payment_log_attrs)
  end

  def payment_log_attrs
    {
      transaction_type: PaypalServiceBase.transaction_type(@params[:txn_type], @params[:payment_status]),
      payload: @params,
      user_id: user.id
    }
  end

  def subscription?
    PaypalServiceBase.transaction_type(@params[:txn_type], @params[:payment_status]) == 'subscription'
  end

  def cancel?
    PaypalServiceBase.transaction_type(@params[:txn_type], @params[:payment_status]) == 'cancel'
  end

  def user
    @user ||= User.find(@params[:custom])
  end

  def payment_date
    @payment_date ||= Date.strptime(@params[:subscr_date], PAY_PAY_DATE_FORMAT)
  end

  def expiration_date
    return payment_date + 12.months if subscription_annual?

    payment_date + 1.month
  end

  def subscription_type
    if @params[:period3].present?
      SUBSCRIPTION_TYPES[@params[:period3].to_sym]
    else
      # default subs if user.account not exists
      return 'M' if user.account.nil?

      user.account.subscription_type
    end
  end

  def subscription_monthly?
    subscription_type == 'M'
  end

  def subscription_annual?
    subscription_type == 'Y'
  end
end
