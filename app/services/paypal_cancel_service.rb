# frozen_string_literal: true

#= PaypalCancelService
class PaypalCancelService < PaypalServiceBase
  def exec
    transaction do
      super
      cancel_subscription
    end
  end

  def cancel_subscription
    user.account = UserAccountStatus.new \
    if user.account.nil?
    user.account.update user_account_attrs
  end

  def user_account_attrs
    {
      user_id: user.id,
      state: 'normal',
      last_state: user.account.state,
      has_subscription: false
    }
  end
end
