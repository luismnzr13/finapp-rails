# frozen_string_literal: true

module ConektaService
  class << self
    def subscription(params, user)
      plan_id = create_plan params
      customer = ConektaService::Customer.create params, user
      customer.create_subscription plan: plan_id
    rescue Conekta::ProcessingError => exception
      ConektaService::Loggable.to_console exception
      raise Exceptions::CardFailedError, exception.details.map(&:message).join('.')
    rescue Conekta::Error => exception
      ConektaService::Loggable.to_console exception
      raise Exceptions::PaymentError, exception.details.map(&:message).join('.')
    end

    def create_plan(params)
      plan_id = Digest::SHA256.hexdigest params[:payment][:token]
      ConektaService::Plan.create id: "subscription-#{plan_id}",
                                  name: "subscription for #{params[:plan][:name]}",
                                  amount: ::Plans.price(params[:plan][:name]) - ::Coupons.discount(params[:coupon][:code], params[:plan][:name]),
                                  currency: 'MXN',
                                  interval: 'month',
                                  frequency: 1,
                                  trial_period_days: 0,
                                  expiry_count: ::Plans.period(params[:plan][:name])
      "subscription-#{plan_id}"
    end
  end
end
